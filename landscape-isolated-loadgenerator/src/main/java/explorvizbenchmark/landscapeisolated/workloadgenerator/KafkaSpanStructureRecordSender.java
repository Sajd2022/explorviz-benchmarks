package explorvizbenchmark.landscapeisolated.workloadgenerator;

import com.google.common.io.BaseEncoding;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer;
import io.opencensus.proto.dump.DumpSpans;
import io.opencensus.proto.trace.v1.Span;
import net.explorviz.avro.SpanStructure;
import net.explorviz.avro.Timestamp;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serdes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import theodolite.commons.workloadgeneration.KafkaRecordSender;
import theodolite.commons.workloadgeneration.RecordSender;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;

public class KafkaSpanStructureRecordSender implements RecordSender<DumpSpans> {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSpanStructureRecordSender.class);

    private final String topic;

    private final Function<SpanStructure, String> keyAccessor;

    private final Function<SpanStructure, Long> timestampAccessor;

    private final Producer<String, SpanStructure> producer;

    private static final int SPAN_ID_LEN = 8;
    private static final int TRACE_ID_LEN = 16;

    /**
     * Create a new {@link KafkaRecordSender}.
     */
    private KafkaSpanStructureRecordSender(final KafkaSpanStructureRecordSender.Builder builder) {
        this.topic = builder.topic;
        this.keyAccessor = builder.keyAccessor;
        this.timestampAccessor = builder.timestampAccessor;

        final Properties properties = new Properties();
        properties.putAll(builder.defaultProperties);
        properties.put("bootstrap.servers", builder.bootstrapServers);
        properties.put("acks", "all");
        properties.put("enable.idempotence", "true");
        // properties.put("batch.size", this.batchSize);
        // properties.put("linger.ms", this.lingerMs);
        // properties.put("buffer.memory", this.bufferMemory);

        final SpecificAvroSerializer<SpanStructure> serializer = new SpecificAvroSerializer<>();
        final Map<String, String> serializerProperties = new HashMap<>();
        serializerProperties.put("schema.registry.url", builder.schemaRegistryUrl);
        serializer.configure(serializerProperties, false);

        this.producer = new KafkaProducer<>(properties, Serdes.String().serializer(), serializer);
    }

    /**
     * Write the passed monitoring record to Kafka.S
     */
    public void write(final SpanStructure monitoringRecord) {
        final ProducerRecord<String, SpanStructure> record =
                new ProducerRecord<>(this.topic, null, this.timestampAccessor.apply(monitoringRecord),
                        this.keyAccessor.apply(monitoringRecord), monitoringRecord);

        LOGGER.info("Send record to Kafka topic {}: {}", this.topic, record);
        try {
            this.producer.send(record);
        } catch (final SerializationException e) {
            LOGGER.warn(
                    "Record could not be serialized and thus not sent to Kafka due to exception. Skipping this record.", // NOCS
                    e);
        }
    }

    public void terminate() {
        this.producer.close();
    }

    @Override
    public void send(DumpSpans message) {
        LOGGER.info("Generated dumpspans message. Sending spans now.");
        for (Span span: message.getSpansList()) {
            try {
                AttributesReader attributesReader = new AttributesReader(span);
                SpanStructure.Builder builder = SpanStructure.newBuilder();
                builder.setSpanId(BaseEncoding.base16().lowerCase().encode(span.getSpanId().toByteArray(), 0, SPAN_ID_LEN));
                builder.setLandscapeToken(attributesReader.getLandscapeToken());
                builder.setTimestamp(new Timestamp(span.getStartTime().getSeconds(), span.getStartTime().getNanos()));
                builder.setHashCode(HashHelper.fromSpanAttributes(attributesReader));
                builder.setFullyQualifiedOperationName(attributesReader.getMethodFqn());
                builder.setHostname(attributesReader.getHostIpAddress());
                builder.setHostIpAddress(attributesReader.getHostIpAddress());
                builder.setAppName(attributesReader.getApplicationName());
                builder.setAppInstanceId(attributesReader.getApplicationInstanceId());
                builder.setAppLanguage(attributesReader.getApplicationLanguage());
                SpanStructure spanStructure = builder.build();

                this.write(spanStructure);
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    public static KafkaSpanStructureRecordSender.Builder builder(
            final String bootstrapServers,
            final String topic,
            final String schemaRegistryUrl) {
        return new KafkaSpanStructureRecordSender.Builder(bootstrapServers, topic, schemaRegistryUrl);
    }

    /**
     * Builder class to build a new {@link KafkaRecordSender}.
     *
     */
    public static class Builder {

        private final String bootstrapServers;
        private final String topic;
        private Function<SpanStructure, String> keyAccessor = x -> ""; // NOPMD
        private Function<SpanStructure, Long> timestampAccessor = x -> null; // NOPMD
        private Properties defaultProperties = new Properties(); // NOPMD
        private final String schemaRegistryUrl;

        /**
         * Creates a Builder object for a {@link KafkaRecordSender}.
         *
         * @param bootstrapServers The Server to for accessing Kafka.
         * @param topic The topic where to write.
         */
        private Builder(final String bootstrapServers, final String topic, final String schemaRegistryUrl) {
            this.bootstrapServers = bootstrapServers;
            this.topic = topic;
            this.schemaRegistryUrl = schemaRegistryUrl;
        }

        public KafkaSpanStructureRecordSender.Builder keyAccessor(final Function<SpanStructure, String> keyAccessor) {
            this.keyAccessor = keyAccessor;
            return this;
        }

        public KafkaSpanStructureRecordSender.Builder timestampAccessor(final Function<SpanStructure, Long> timestampAccessor) {
            this.timestampAccessor = timestampAccessor;
            return this;
        }

        public KafkaSpanStructureRecordSender.Builder defaultProperties(final Properties defaultProperties) {
            this.defaultProperties = defaultProperties;
            return this;
        }

        public KafkaSpanStructureRecordSender build() {
            return new KafkaSpanStructureRecordSender(this);
        }
    }
}